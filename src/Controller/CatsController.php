<?php

namespace App\Controller;

use App\Entity\Cats;
use App\Form\CatsType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/cats')]
class CatsController extends AbstractController
{
    #[Route('/', name: 'app_cats_index', methods: ['GET'])]
    public function index(EntityManagerInterface $entityManager): Response
    {
        $cats = $entityManager
            ->getRepository(Cats::class)
            ->findAll();

        return $this->render('cats/index.html.twig', [
            'cats' => $cats,
        ]);
    }

    #[Route('/new', name: 'app_cats_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $cat = new Cats();
        $form = $this->createForm(CatsType::class, $cat);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($cat);
            $entityManager->flush();

            return $this->redirectToRoute('app_cats_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('cats/new.html.twig', [
            'cat' => $cat,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_cats_show', methods: ['GET'])]
    public function show(Cats $cat): Response
    {
        return $this->render('cats/show.html.twig', [
            'cat' => $cat,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_cats_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Cats $cat, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(CatsType::class, $cat);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_cats_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('cats/edit.html.twig', [
            'cat' => $cat,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_cats_delete', methods: ['POST'])]
    public function delete(Request $request, Cats $cat, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$cat->getId(), $request->request->get('_token'))) {
            $entityManager->remove($cat);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_cats_index', [], Response::HTTP_SEE_OTHER);
    }
}
